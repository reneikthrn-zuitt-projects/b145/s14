//displays the message 
console.log("Hello from JS"); 

// this is a single-line commment 

/* multi-line commment
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
*/

// ctrl + /
// console.log("end of comment"); 

console.log("example statement");

//syntax: console.log(value/message);

//[DECLARING VARIABLES]
// -> tell our devices that a variable name is created and ready to store data.

// syntax: let/const desiredVariableName;

let myVariable; 

//WHAT is a variable?
	// ==> It is used to contain/store data.

//Benefits of utilizing a variable?
    //This makes it it easier for us to associate information stored in our devices to actual names about the information.

 // an assignment operator (=) is used to assign/pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNumber = "09951446335";

//[PEEKING INSIDE A VARIABLE]
let greetings;

console.log(clientName); 
console.log(contactNumber); 
console.log(greetings);
let pangalan = "John Doe"; 
console.log(pangalan);



/*
	SESSION 14 - DECEMBER 03, 2021
*/

// let's create a collection of all your subjects in the bootcamp
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "JavaScript"];

// display the output of the array inside the console
console.log(bootcampSubjects);

/* 
	Rule of Thumb: wheen declaring array structures
	=> Storing multiple data types inside an array is NOT recommended. In context, of programming this does not make any sense.
	=> An array should be a collection of data that describes a similar/single topic or subject.
*/
let details = ["Keanu", "Reeves", 32, true];
console.log(details);

/*
	Objects
		- are another special kind of composite data type that is used to mimic or represent real world object/item.
		- they are used to create complex data that contains pieces of information that are relevant to each other.
		- every individual piece of code/information is called property of an object

	BASIC SYNTAX:
		let/const objectName = {
			(key -> value)
			propertyA: value,
			propertyB: value
		}
*/

// let's create an object that describes the properties of a cellphone.
let cellphone = { 
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX120021222',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000
}

// let;s display the object inside the console
console.log(cellphone);

/*
	VARIABLES AND CONSTANTS
	Variables - used to store data
	Constants - permanent, fixed, absolute
*/

/* 
	Variables
		=> the values/info stored inside a variable can be changed or repacked.
*/
let personName = "Michael";
console.log(personName);

// if you're going to reassign a new value for a variable, you no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

// concatenating string => join, combine, link
let pet = "dog";
console.log("This is the initial value of var: " + pet);

pet = "cat";
console.log("This is the new value of var: " + pet);


/* Constants
		=> The value assigned on constant cannot be changed
	BASIC SYNTAX:
	const desiredName = value;
*/
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

// let's try to reassign a new value in a constant.
// const familyName = 'Castillo';
// console.log(familyName);
